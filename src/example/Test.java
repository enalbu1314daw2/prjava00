package example;

import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Created with IntelliJ IDEA.
 * User: rufo
 * Date: 10/14/13
 * Time: 5:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class Test {
    public static void main(String[] args){
        System.out.println("Hola món");
        System.out.println("Versió 0.2 del projecte prjava00");
        try{
            InetAddress addr=InetAddress.getLocalHost();
            byte[] ipAddr=addr.getAddress();
            String hostname = addr.getHostName();
            System.out.println("hostname="+hostname);
            System.out.println("Nom de l' usuari: "+System.getProperty("user.name"));
            System.out.println("Carpeta personal: "+System.getProperty("user.home"));
            System.out.println("Sistema operatiu: "+System.getProperty("os.name"));
            System.out.println("Versió OS: "+System.getProperty("os.version"));
        }catch (UnknownHostException e){
             e.printStackTrace();
        }
    }
}
